#!/usr/bin/env bash
set -e
bundle exec rubocop
bundle exec bundle-audit
bundle exec brakeman --run-all-checks --exit-on-warn .
