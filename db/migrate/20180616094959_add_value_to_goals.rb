class AddValueToGoals < ActiveRecord::Migration[5.1]
  def change
    add_column :goals, :value, :integer
  end
end
