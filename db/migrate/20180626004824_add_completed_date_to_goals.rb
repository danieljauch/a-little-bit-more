class AddCompletedDateToGoals < ActiveRecord::Migration[5.1]
  def change
    add_column :goals, :completed_date, :date
  end
end
