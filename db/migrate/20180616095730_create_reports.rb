class CreateReports < ActiveRecord::Migration[5.1]
  def change
    create_table :reports do |t|
      t.date :reported_date
      t.integer :value
      t.string :alternate_unit
      t.text :note

      t.timestamps
    end
  end
end
