class AddUnitToGoals < ActiveRecord::Migration[5.1]
  def change
    add_column :goals, :unit, :string
  end
end
