class AddTitleToGoals < ActiveRecord::Migration[5.1]
  def change
    add_column :goals, :title, :text
  end
end
