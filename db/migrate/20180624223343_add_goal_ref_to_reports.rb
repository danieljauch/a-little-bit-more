class AddGoalRefToReports < ActiveRecord::Migration[5.1]
  def change
    add_reference :reports, :goal, foreign_key: true
  end
end
