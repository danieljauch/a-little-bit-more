Rails.application.routes.draw do
  devise_for :users

  resources :users, only: [:show]
  resources :goals
  resources :reports, except: %i[index show]

  get "/profile" => "users#show"

  root "goals#index"
end
