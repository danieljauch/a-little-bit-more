# A Little Bit More

If you've ever achieved a goal, you know that it doesn't happen all at once. The journey of 1,000 miles begins with the first step, right?

Goals are achieved when every day you do **a little bit more**.

This app helps you track your goals, gives you the ability to add incremental progress, and shows your trend line to keep you on track.