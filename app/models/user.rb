class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :goals
  has_many :reports, through: :goals

  def goal_count
    goals.length
  end

  def complete_goal_count
    goals.count(&:complete?)
  end

  def reports_count
    reports.length
  end

  def next_level
    level + 1
  end

  def next_level_base
    Level.level_base(next_level)
  end

  def change_experience(dif)
    new_xp = experience + dif
    if dif.positive?
      self.level = Level.check_level(new_xp)
      self.experience = new_xp
    elsif new_xp > Level.level_base(level)
      self.experience = new_xp
    else
      self.experience = Level.level_base(level)
    end
    save!
  end

  def progress_percentage
    "#{((experience - Level.level_base(level)).to_f / (Level.level_base(next_level) - Level.level_base(level)).to_f * 100).to_i}%"
  end
end
