# frozen_string_literal: true

class Goal < ApplicationRecord
  belongs_to :user
  has_many :reports

  validates :title, presence: true
  validates :value, presence: true
  validates :unit, presence: true
  validates :user_id, presence: true

  def full_sentence_output
    if end_date
      "#{title} #{value} #{unit} by #{DateOutput.date_output(end_date)}"
    else
      "#{title} #{value} #{unit}"
    end
  end

  def status_class
    status.tr(" ", "-")
  end

  def total_progress
    reports.sum(&:value)
  end

  def progress_percentage
    "#{(total_progress.to_f / value.to_f * 100).to_i}%"
  end

  def days_taken
    (Time.now.to_i - Time.at(created_at).to_i) / (60 * 60 * 24)
  end

  def days_left
    (end_date - Time.zone.today).to_i
  end

  def pace_output
    "#{pace} #{unit} / day" unless days_taken.zero?
  end

  def chart_data
    labels = reports.map { |report| DateOutput.date_output(report.reported_date) }
    data = reports.map(&:value)
    
    ChartBuilder.chart_data(labels, data)
  end

  def chart_options
    ChartBuilder.chart_options("report", self.id)
  end

  def status
    if complete?
      if completed_date?
        if completed_date >= end_date
          "complete on #{DateOutput.date_output(completed_date)}"
        else
          "complete early on #{DateOutput.date_output(completed_date)}"
        end
      else
        "complete"
      end
    elsif estimated_end_date < end_date
      "ahead"
    elsif estimated_end_date == end_date
      "on pace"
    else
      "behind"
    end
  end

  def complete?
    total_progress >= value
  end

  def estimated_end_date
    unless days_taken.zero?
      remainder = value - total_progress

      if remainder.positive? && pace.positive?
        Time.zone.today + (remainder / pace)
      else
        Time.zone.today
      end
    end
    end_date
  end

  private

  def pace
    total_progress / days_taken
  end

  def past_due?
    Date.today > end_date
  end
end
