class Report < ApplicationRecord
  belongs_to :goal

  validates :value, presence: true
end
