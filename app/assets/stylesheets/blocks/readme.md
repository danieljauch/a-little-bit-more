# Blocks:

**Purpose:** Style objects that will show up on multiple pages in a way such that the page doesn't effect the stylings or elements present
**Preferred load order:** Traditionally alphabetical, order agnostic
**Global load order:** Fifth, after objects