# Layouts:

**Purpose:** Set up blocks that will be on every page with breakpoints. Used mainly for positioning so that the contents of the large scale blocks don't have to worry about size or position and can simply be dynamic to viewport and container widths.
**Preferred load order:** Order that they show up on the page. Traditionally: header, main, aside, footer
**Global load order:** Third, after global tag stylings