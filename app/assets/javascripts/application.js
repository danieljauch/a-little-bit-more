//= require rails-ujs
//= require turbolinks
//= require jquery
//= require Chart.min
//= require_tree .

$(document).ready(function () {
  if ($('.dismiss-alert-btn').length > 0) {
    setTimeout(function () {
      $('.alert').addClass('dismissed');
    }, 2500);
  }

  $('.dismiss-alert-btn').click(function () {
    $('.alert').addClass('dismissed');
  });

  $('.mobile-menu-toggle').click(function () {
    $('.site-menu').toggleClass('open');
  });
});
