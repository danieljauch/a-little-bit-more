class GoalsController < ApplicationController
  before_action :set_goal, only: %i[show edit update destroy]

  def index
    @goals = Goal.where(user_id: current_user.id) if current_user
  end

  def show; end

  def new
    @goal = Goal.new
  end

  def create
    goal = Goal.new(goal_params)
    goal.title.capitalize!
    goal.unit.downcase!
    goal.user_id = current_user.id

    return redirect_to goals_url, notice: "Goal successfully created." if goal.save
    render :new
  end

  def edit; end

  def update
    return redirect_to @goal, notice: "Goal successfully updated." if @goal.update(goal_params)
    render :edit
  end

  def destroy
    @goal.destroy
    redirect_to goals_url, notice: "Goal successfully deleted."
  end

  private

  def set_goal
    @goal = current_user.goals.find(params[:id])
  end

  def goal_params
    params.require(:goal).permit(:title, :value, :unit, :end_date)
  end
end
