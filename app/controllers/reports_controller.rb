class ReportsController < ApplicationController
  before_action :set_report, only: %i[edit update destroy]
  before_action :set_goals, only: %i[new edit]

  def new
    @report = Report.new
  end

  def edit; end

  def create
    report = Report.new(report_params)

    if ProcessNewReport.process(report, current_user)
      redirect_to goals_path, notice: "Report successfully logged."
    else
      render :new
    end
  end

  def update
    return redirect_to goals_path, notice: "Report successfully updated." if @report.update(report_params)
    render :edit
  end

  def destroy
    @report.destroy
    redirect_to goals_path, notice: "Report successfully deleted."
  end

  private

  def set_report
    @report = current_user.reports.find(params[:id])
  end

  def set_goals
    @goals = Goal.where(user_id: current_user.id) if current_user
  end

  def report_params
    params.require(:report).permit(:reported_date, :value, :alternate_unit, :note, :goal_id)
  end
end
