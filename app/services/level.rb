class Level
  def self.level_shape(level)
    if level < 25
      "circle"
    elsif level < 50
      "triangle"
    elsif level < 75
      "pentagon"
    elsif level < 100
      "hexagon"
    else # level == 100
      "chevron"
    end
  end

  def self.level_base(level)
    if level >= 1 && level < 6
      ((level - 1) * level / 2) * 10
    elsif level >= 6 && level < 10
      100 + ((level - 5) * 50)
    elsif level >= 11 && level < 15
      350 + ((level - 10) * 100)
    elsif level >= 16 && level < 20
      850 + ((level - 15) * 150)
    elsif level >= 21 && level < 25
      1600 + ((level - 20) * 200)
    elsif level >= 26 && level < 30
      2600 + ((level - 25) * 250)
    elsif level >= 31 && level < 35
      3850 + ((level - 30) * 300)
    elsif level >= 36 && level < 40
      5350 + ((level - 35) * 350)
    elsif level >= 41 && level < 45
      7100 + ((level - 40) * 400)
    elsif level >= 46 && level < 50
      9100 + ((level - 45) * 450)
    elsif level >= 51 && level < 55
      11_350 + ((level - 50) * 500)
    elsif level >= 56 && level < 60
      13_850 + ((level - 55) * 550)
    elsif level >= 61 && level < 65
      16_600 + ((level - 60) * 600)
    elsif level >= 66 && level < 70
      19_600 + ((level - 65) * 650)
    elsif level >= 71 && level < 75
      22_850 + ((level - 70) * 700)
    elsif level >= 76 && level < 80
      26_350 + ((level - 75) * 750)
    elsif level >= 81 && level < 85
      30_100 + ((level - 80) * 800)
    elsif level >= 86 && level < 90
      34_100 + ((level - 85) * 850)
    elsif level >= 91 && level < 95
      38_350 + ((level - 90) * 900)
    elsif level >= 96 && level < 100
      42_850 + ((level - 95) * 950)
    else # level === 100
      50_000
    end
  end

  def self.check_level(xp)
    if xp >= 50_000
      100
    elsif xp >= 42_850
      ((xp - 42_850) / 950) + 95
    elsif xp >= 38_350
      ((xp - 38_350) / 900) + 90
    elsif xp >= 34_100
      ((xp - 34_100) / 850) + 85
    elsif xp >= 30_100
      ((xp - 30_100) / 800) + 80
    elsif xp >= 26_350
      ((xp - 26_350) / 750) + 75
    elsif xp >= 22_850
      ((xp - 22_850) / 700) + 70
    elsif xp >= 19_600
      ((xp - 19_600) / 650) + 65
    elsif xp >= 16_600
      ((xp - 16_600) / 600) + 60
    elsif xp >= 13_850
      ((xp - 13_850) / 550) + 55
    elsif xp >= 11_350
      ((xp - 11_350) / 500) + 50
    elsif xp >= 9100
      ((xp - 9100) / 450) + 45
    elsif xp >= 7100
      ((xp - 7100) / 400) + 40
    elsif xp >= 5350
      ((xp - 5350) / 350) + 35
    elsif xp >= 3850
      ((xp - 3850) / 300) + 30
    elsif xp >= 2600
      ((xp - 2600) / 250) + 25
    elsif xp >= 1600
      ((xp - 1600) / 200) + 20
    elsif xp >= 850
      ((xp - 850) / 150) + 15
    elsif xp >= 350
      ((xp - 350) / 100) + 10
    elsif xp >= 100
      ((xp - 100) / 50) + 5
    elsif xp >= 60
      4
    elsif xp >= 30
      3
    elsif xp >= 10
      2
    else
      1
    end
  end
end
