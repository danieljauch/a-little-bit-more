class DateOutput
  def self.date_output(date_attr)
    date_attr.strftime("%b %d")
  end
end
