class ProcessNewReport
  def self.process(report, current_user)
    goal = Goal.find(report.goal_id)
    report.save

    check_complete(goal)
    add_experience(goal, current_user)
  end

  def self.check_complete(goal)
    goal.completed_date = Time.zone.today if goal.complete?
  end

  def self.add_experience(goal, user)
    experience = 1
    experience += 10 if goal.complete?
    experience *= 2 if weekend?

    user.change_experience(experience)
  end

  def self.weekend?
    Time.zone.today.saturday? || Time.zone.today.sunday?
  end
end
