class ChartBuilder
  def self.chart_data (labels, data)
    {
      labels: labels,
      datasets: [
        {
          label: "Reports",
          backgroundColor: "rgba(0, 170, 42, 0.5)",
          borderColor: "#228039",
          data: data
        }
      ]
    }
  end

  def self.chart_options (obj_name, obj_id)
    {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      },
      class: "#{obj_name}-chart",
      id: "#{obj_name}_chart_#{obj_id}",
      width: "100%",
      height: "32px"
    }
  end
end
