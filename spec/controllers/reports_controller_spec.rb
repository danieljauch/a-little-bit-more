require "rails_helper"

RSpec.describe ReportsController, type: :controller do
  describe "#new" do
    subject { post :new }

    it "renders new page" do
      expect(subject).to render_template(:new)
    end
  end

  describe "#edit" do
    before { FactoryBot.create(:report) }
    subject { get :edit, params: { id: Report.last.id } }

    it "renders edit page" do
      expect(subject).to render_template(:edit)
    end
  end

  # describe "#create" do
  #   subject { post :create, params: { report: FactoryBot.attributes_for(:report), goal_id: Goal.last.id } }

  #   it "redirects to goals page on creation" do
  #     expect(subject).to redirect_to(goals_path)
  #   end
  # end

  # context "after creation" do
  #   let!(:report) { FactoryBot.create(:report) }

  #   describe "#update" do
  #     subject { put :update, params: { id: report.id, value: 2 } }

  #     it "redirects to goals page on update" do
  #       expect(subject).to redirect_to(goals_path)
  #     end
  #   end

  #   describe "#destroy" do
  #     subject { delete :destroy, params: { id: report.id } }

  #     it "redirects to goals page on deletion" do
  #       expect(subject).to redirect_to(goals_path)
  #     end
  #   end
  # end
end
