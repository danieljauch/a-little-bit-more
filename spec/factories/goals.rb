FactoryBot.define do
  factory :goal do
    user_id 1
    title "Run"
    value "1000"
    unit "miles"
    end_date 14.days.from_now

    association :user

    trait :draft do
      # default
    end

    trait :with_single_report do
      created_at Time.zone.yesterday

      after(:build) do |goal, _evaluator|
        create(:report, goal: goal)
      end
    end

    trait :with_multiple_reports do
      created_at 2.days.ago
      
      transient do
        reports_count 2
      end

      after(:build) do |goal, evaluator|
        create_list(:report, evaluator.reports_count, goal: goal)
      end
    end

    trait :complete do
      created_at Time.zone.yesterday
      
      transient do
        reports_count 100
      end

      after(:build) do |goal, evaluator|
        create_list(:report, evaluator.reports_count, goal: goal)
      end
    end
  end
end
