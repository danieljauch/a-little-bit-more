FactoryBot.define do
  factory :report do
    reported_date Time.zone.today
    value 10

    association :goal
  end
end
