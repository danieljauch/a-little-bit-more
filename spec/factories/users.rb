FactoryBot.define do
  factory :user do
    email Faker::Internet.email
    password '1Ab.56'
    password_confirmation '1Ab.56'
  end
end
