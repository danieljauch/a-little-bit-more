require "rails_helper"

RSpec.describe User, type: :model do
  subject { FactoryBot.create(:user) }

  it "has a valid factory" do
    expect(subject).not_to be_nil
  end

  describe "with a valid sign up" do
    it "has a valid email" do
      expect(subject.email).not_to be_nil
    end

    it "has a valid password" do
      expect(subject.password).not_to be_nil
    end
  end

  # describe 'with an invalid email' do
  #   subject { FactoryBot.create(:bad_user_email) }

  #   it 'raises error' do
  #     expect(subject.email).to raise_error
  #   end
  # end

  # describe 'with an invalid password' do
  #   subject { FactoryBot.create(:bad_user_password) }

  #   it 'raises error' do
  #     expect(subject.password).to raise_error
  #   end
  # end

  # describe 'with an invalid password confirmation' do
  #   subject { FactoryBot.create(:bad_user_password_confirmation) }

  #   it 'raises error' do
  #     expect(subject.password_confirmation).to raise_error
  #   end
  # end

  describe "after sign up" do
    it "has no goals" do
      expect(subject.goals).to eq []
    end

    it "has no goal count" do
      expect(subject.goal_count).to eq 0
    end

    it "has no complete goal" do
      expect(subject.complete_goal_count).to eq 0
    end

    it "has no reports" do
      expect(subject.reports_count).to eq 0
    end

    it "level is 1" do
      expect(subject.level).to eq 1
    end

    it "experience is 0" do
      expect(subject.experience).to eq 0
    end
  end

  # describe 'with goals' do
  # end

  # describe 'with reports' do
  # end

  describe "before first level up" do
    it "knows next level" do
      expect(subject.next_level).to eq 2
    end

    it "knows next level base" do
      expect(subject.next_level_base).to eq 10
    end

    it "calculates its level progress percentage properly" do
      expect(subject.progress_percentage).to eq "0%"
    end
  end

  describe "after first level up" do
    before { subject.change_experience(15) }

    it "changes experience properly" do
      expect(subject.experience).to eq 15
    end

    it "changes level properly" do
      expect(subject.level).to eq 2
    end

    it "still knows next level" do
      expect(subject.next_level).to eq 3
    end

    it "still knows next level base" do
      expect(subject.next_level_base).to eq 30
    end

    it "still calculates its level progress percentage properly" do
      expect(subject.progress_percentage).to eq "#{(5.to_f / 20.to_f * 100).to_i}%"
    end
  end

  describe "after losing experience" do
    before { subject.change_experience(-1) }

    it "changes experience properly" do
      expect(subject.experience).to eq 0
    end

    it "changes level properly" do
      expect(subject.level).to eq 1
    end

    it "still knows next level" do
      expect(subject.next_level).to eq 2
    end

    it "still knows next level base" do
      expect(subject.next_level_base).to eq 10
    end

    it "still calculates its level progress percentage properly" do
      expect(subject.progress_percentage).to eq "0%"
    end
  end
end
