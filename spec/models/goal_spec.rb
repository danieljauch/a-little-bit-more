require "rails_helper"

RSpec.describe Goal, type: :model do
  subject { FactoryBot.create(:goal) }

  it "has a valid factory" do
    expect(subject).not_to be_nil
  end

  it "has a valid title" do
    expect(subject.title).not_to be_nil
  end

  it "has a valid value" do
    expect(subject.value).not_to be_nil
  end

  it "has a valid unit" do
    expect(subject.unit).not_to be_nil
  end

  it "has a valid end date" do
    expect(subject.end_date).to be > Time.zone.today
  end

  it "outputs dates properly" do
    expect(DateOutput.date_output(subject.end_date)).to eq subject.end_date.strftime("%b %d")
  end

  it "has a full sentence output" do
    expect(subject.full_sentence_output).to eq(
      "#{subject.title} #{subject.value} #{subject.unit} by #{DateOutput.date_output(subject.end_date)}"
    )
  end

  context "before having reports" do
    let!(:goal) { FactoryBot.create(:goal, :draft) }

    it "calculates progress properly" do
      expect(goal.total_progress).to eq 0
    end

    it "calculates progress percentage properly" do
      expect(goal.progress_percentage).to eq "0%"
    end

    it "recognizes no days have been taken" do
      expect(goal.days_taken).to eq 0
    end

    it "does not ouptut pace" do
      expect(goal.pace_output).to be_nil
    end

    it "states reports as on pace" do
      expect(goal.status).to eq "on pace"
    end

    it "claims itself incomplete" do
      expect(goal.complete?).to eq false
    end

    it "estimates its end date as the predefined end date" do
      expect(goal.estimated_end_date).to eq goal.end_date
    end
  end

  context "after having one report and one day" do
    let!(:goal) { FactoryBot.create(:goal, :with_single_report) }

    it "calculates progress properly" do
      expect(goal.total_progress).to eq 10
    end

    it "calculates progress percentage properly" do
      expect(goal.progress_percentage).to eq "1%"
    end

    it "recognizes 1 day has been taken" do
      expect(goal.days_taken).to eq 1
    end

    it "outputs pace properly" do
      expect(goal.pace_output).to eq "10 miles / day"
    end

    it "states reports as behind" do
      expect(goal.status).to eq "behind"
    end

    it "claims itself incomplete" do
      expect(goal.complete?).to eq false
    end

    it "estimates its end date properly" do
      expect(goal.estimated_end_date).to eq Time.zone.yesterday + 100
    end
  end

  context "after having multiple reports and multiple days" do
    let!(:goal) { FactoryBot.create(:goal, :with_multiple_reports) }

    it "calculates progress properly" do
      expect(goal.total_progress).to eq 20
    end

    it "calculates progress percentage properly" do
      expect(goal.progress_percentage).to eq "2%"
    end

    it "recognizes multiple days have been taken" do
      expect(goal.days_taken).to be > 1
    end

    it "outputs pace properly" do
      expect(goal.pace_output).to eq "10 miles / day"
    end

    it "states reports as behind" do
      expect(goal.status).to eq "behind"
    end

    it "claims itself incomplete" do
      expect(goal.complete?).to eq false
    end

    it "estimates its end date properly" do
      expect(goal.estimated_end_date).to eq Time.zone.today + 98
    end
  end

  context "after being completed" do
    let!(:goal) { FactoryBot.create(:goal, :complete) }

    it "calculates progress properly" do
      expect(goal.total_progress).to eq 1000
    end

    it "calculates progress percentage properly" do
      expect(goal.progress_percentage).to eq "100%"
    end

    it "states reports as complete" do
      expect(goal.status).to eq "complete"
    end

    it "claims itself complete" do
      expect(goal.complete?).to eq true
    end
  end
end
