require "rails_helper"

RSpec.describe Report, type: :model do
  let(:goal) { FactoryBot.create(:goal) }

  subject { FactoryBot.create(:report) }

  it "has a valid factory" do
    expect(subject).not_to be_nil
  end

  it "belongs to a goal" do
    expect(subject.goal_id).not_to be_nil
  end

  it "has a value" do
    expect(subject.value).not_to be_nil
  end

  it "has a positive value" do
    expect(subject.value).to be > 0
  end

  it "outputs reported date in correct format" do
    expect(DateOutput.date_output(subject.reported_date)).to eq subject.reported_date.strftime("%b %d")
  end

  # describe 'after changing goal'
  #   before { subject.goal_id = 2 }
  # end
end
